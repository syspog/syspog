## How to use

#### Superuser
`sudo su`

#### Install driver alsa
`apt-get install libasound2 alsa-utils alsa-oss`

#### Install aplikasi untuk memutar mp3
`apt-get install ffmpeg`

#### Git clone 
`https://gitlab.com/syspog/syspog`

#### Menghubungkan virtualbox dengan terminal
`ssh user@localhost -p 6022`

#### Memindahkan file `manage.sh` & `manage.service`
```
mv manage.sh /root
mv manage.service /lib/systemd/system
```

#### Menghidupkan service 
```
systemctl daemon-reload
systemctl enable manage.service
systemctl start manage.service
```

#### Restart

Untuk bisa melakukan login, pengguna perlu keluar dari script yang sedang berjalan dengan memasukan opsi 3 pada menu utama. Setelah itu pengguna dapat melakukan ssh ke VBox setelah keluar dari bootscript. Untuk menjalankan kembali script ini dapat dilakukan dengan eskekusi "sh /root/manage.sh" atau "systemctl start manage.service" pada terminal ssh.
