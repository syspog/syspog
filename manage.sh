#!/bin/sh

network_card_advertisement () {
    echo "-------------------------------------------------------------"
    read -r -p "Change enp0s3 Advertisement Mode
    1. Change enp0s3 advertisement to 10baseT Full Duplex AutoNeg On
    2. Change enp0s3 advertisement to 100baseT Full Duplex AutoNeg On
    3. Change enp0s3 advertisement to 1000baseT Full Duplex AutoNeg On
    4. Back
    choose 1-4 : " var2    

    case $var2 in
        1)
            ethtool -s enp0s3 speed 10 duplex full autoneg on	
            ;;		 
        2)
            ethtool -s enp0s3 speed 100 duplex full autoneg on
            ;;
        3)
            ethtool -s enp0s3 speed 1000 duplex full autoneg on
            ;;
        4)
            ;;   
    esac
    
    ethtool enp0s3 | grep "Advertised link modes:"
}



network_card_rx_tx () {
    echo "---------------------------------------------------------------------------------"
            read -r -p "Change enp0s3 RX/TX Buffer Size
            1. Change enp0s3 RX/TX Buffer to 4096/4096
            2. Change enp0s3 RX/TX Buffer to 2048/2048
            3. Change enp0s3 RX/TX Buffer to 1024/1024
            4. Change enp0s3 RX/TX Buffer to 512/512
            5. Change enp0s3 RX/TX Buffer to 256/256
            6. Back
            choose 1-6 : " var2    
	        case $var2 in
                1)
                    ethtool -G enp0s3 rx 4096 tx 4096			 
                    echo "Change enp0s3 RX/TX Buffer to 4096/4096"
                    ;;

                2)
                    ethtool -G enp0s3 rx 2048 tx 2048		
                    echo "Change enp0s3 RX/TX Buffer to 2048/2048"
                    ;;

                3)
                    ethtool -G enp0s3 rx 1024 tx 1024	
                    echo "Change enp0s3 RX/TX Buffer to 1024/1024"
                    ;;

                4)  
                    ethtool -G enp0s3 rx 512 tx 512	
                    echo "Change enp0s3 RX/TX Buffer to 512/512"
                    ;;

                5)  
                    ethtool -G enp0s3 rx 256 tx 256	
                    echo "Change enp0s3 RX/TX Buffer to 256/256"
                    ;;

                6) 
                    ;;        
            esac
}

network_card () {
    echo "---------------------------------------------------------------------------------"
    echo "Choose enp0s3 Network Settings"
    echo 
    echo "1. Change Advertisement Mode"
    echo "2. Change RX/TX Buffer"
    echo "3. Exit"
    read -r -p "Choose 1-3 :" var1

    case $var1 in
        1)
            network_card_advertisement
            ;;
        2)
            network_card_rx_tx
            ;;
        3)
			true
            ;;
    esac
    echo "---------------------------------------------------------------------------------"
}

audio_volume () {
    echo "---------------------------------------------------------------------------------"
    echo "Choose Audio Volume"
    echo 
    echo "1. Volume 50%"
    echo "2. Volume 75%"
    echo "3. Volume 100%"
    echo "4. Back"
    read -r -p "Choose 1-3 :" volume

    case $volume in
            1)  
                amixer set 'Master' 50%
                echo "Set Volume to 50%"
                ;;
            2)  
                amixer set 'Master' 75%
                echo "Set Volume to 75%"
                ;;
            3)
                amixer set 'Master' 100%
                echo "Set Volume to 100%"
                ;;
            4)
                true
                ;;
    esac
    echo
    echo "---------------------------------------------------------------------------------" 
}

while :
do
echo "Audio and network Settings"
echo 
echo "1. Network Card"
echo "2. Audio"
echo "3. Exit"
read -r -p "Choose 1-3 :" var1

case $var1 in
        1)
            network_card
            continue;;
        2)
            audio_volume
            continue;;
        3)
			echo "Bye Bye...."
            exit 0;;
    esac
done
